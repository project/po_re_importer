<?php

/**
 * Implements hook_drush_command().
 */
function po_re_importer_drush_command() {
  $items = array();
  $items['po_re_importer_rebuild'] = array(
    'description' => 'Rebuild the index - all translations directories inside the modules, themes and so on directories will scan and updated/new translation files will include in the Po-Re-Importer index.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('porirb'),
  );

  $items['po_re_importer_count'] = array(
    'description' => 'Get the count of already imported/new translation files of a language.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('poric'),
    'arguments' => array(
      'langcode' => 'The langcode of a language. eg de',
      'status' => '0 for not imported (default); 1 for imported (optional)',
    ),
  );

  $items['po_re_importer_import'] = array(
    'description' => 'Import translation files.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('porii'),
    'arguments' => array(
      'module' => 'A drupal module. (eg views_ui from the views project) or following options' . "\n".
                   'all - all translations which already imported will be once again imported.' . "\n".
                   'allnew - all translations which not yet imported will be imported.' . "\n".
                   'del - all translations for the given language and selected textgroups will be deleted from the database first, and you will loose all your customized translations and those not available in the files being imported. Use with extreme caution.'
      ,
      'langcode' => 'The langcode of a language. eg de',
    ),
    'options' => array(
      '--import_mode' => 'The import mode. eg. keep or over.' . "\n" .
                         'keep - Existing strings are kept, only new strings are added (default)'. "\n".
                         'over - Strings in the imported file(s) replace existing ones, new ones are added',
      '--textgroup'   => 'Imported translations will be added to this text group. (default textgroup is default)',
    ),
    'examples' => array(
      'drush porii allnew de' => 'Import all new translation files in the german language if available.',
      'drush porii views hu --import_mode=over' => 'Import all translation files from the views module in the hungarian language and strings in the imported file(s) replace existing ones, new ones are added.',
      'drush porii del fr --import_mode=over' => 'All translations in the french language and selected textgroups will be deleted from the database first, and you will loose all your customized translations and those not available in the files being imported. Use with extreme caution.',
    ),
  );
  return $items;
}

function drush_po_re_importer_rebuild_validate() {
  if (!module_exists('po_re_importer')) {
     return drush_set_error('PO_RE_IMPORTER_NOT_ENABLED', dt('You must enable the Po-Re-Importer to use his commands'));
  }
}

function drush_po_re_importer_rebuild() {
  _po_re_importer_rebuild_index(array(), array(), TRUE);
}

function drush_po_re_importer_count($langcode = '', $status = 0) {
  if (empty($langcode)) {
    return drush_set_error('DRUSH_NO_PROJECT', dt('Please enter a langcode.'));
  }
  if (!$code = _po_re_importer_validate_langcode($langcode)) {
    return drush_set_error('DRUSH_INVALID_LANGCODE', dt('The langcode is invalid.'));
  }
  switch ($status) {
    case 0:
      $count = _po_re_importer_file_count(PO_RE_IMPORTER_NOT_IMPORTED, $langcode);
      if ($count === 0) {
        return drush_print(dt('There are no new translation files in the index'));
      }
      return drush_print(_po_re_importer_file_count_msg($count));
      break;
    case 1:
      $count = _po_re_importer_file_count(PO_RE_IMPORTER_IMPORTED, $langcode);
      if ($count === 0) {
        return drush_print(dt('There are no translation files in the index'));
      }
      return drush_print(format_plural($count, 'There is a translation file in the index.' , 'There are @count translation files in the index.'));
      break;
  }

}

function drush_po_re_importer_import_validate($module = '', $langcode = '') {
  if (!module_exists('po_re_importer')) {
     return drush_set_error('PO_RE_IMPORTER_NOT_ENABLED', dt('You must enable the Po-Re-Importer to use his commands'));
  }

  if (empty($module)) {
    return drush_set_error('DRUSH_NO_PROJECT', dt('Please enter a module or "all", "allnew", "del".'));
  }
  if (empty($langcode)) {
    return drush_set_error('DRUSH_NO_PROJECT', dt('Please enter a langcode.'));
  }

  $modes = array('keep', 'over');
  if (($option = drush_get_option('import_mode')) && !in_array($option, $modes)) {
    return drush_set_error('DRUSH_INVALID_MODE', dt('The import mode is invalid.'));
  }

  $textgroups = array_keys(module_invoke_all('locale', 'groups'));
  if (($option = drush_get_option('textgroup')) && !in_array($option, $textgroups)) {
    return drush_set_error('DRUSH_INVALID_TEXTGROUP', dt('The textgroup is invalid.'));
  }

  $mode = drush_get_option('import_mode');
  switch ($mode) {
    case 'keep':
      $safe_mode = LOCALE_IMPORT_KEEP;
    break;
    case 'over':
      $safe_mode = LOCALE_IMPORT_OVERWRITE;
    break;
    default:
      $safe_mode = LOCALE_IMPORT_KEEP;
    break;
  }

  $group = drush_get_option('textgroup');
  if (empty($group)) {
    $safe_group = 'default';
  }
  else{
    $safe_group = $group;
  }

  if (!$code = _po_re_importer_validate_langcode($langcode)) {
    return drush_set_error('DRUSH_INVALID_LANGCODE', dt('The langcode is invalid.'));
  }

  switch ($module) {
    case 'all':
      if (!variable_get('po_re_importer_imported_files', 0)) {
        return drush_set_error('DRUSH_PROJECT_ARGUMENT_INVALID', dt('Please enable the option "Check this if you want also already imported translation files in the select list" in the Po-Re-Importer settings of your drupal website.'));
      }
      if (!($return = _po_re_importer_file_count(PO_RE_IMPORTER_IMPORTED, $langcode))) {
        return drush_set_error('DRUSH_PROJECT_ARGUMENT_INVALID', dt('There are no modules which have translation files in this language.'));
      }
    break;
    case 'allnew':
      if (!($return = _po_re_importer_file_count(PO_RE_IMPORTER_NOT_IMPORTED, $langcode))) {
        return drush_set_error('DRUSH_PROJECT_ARGUMENT_INVALID', dt('There are no new translation files in this language.'));
      }
    break;
    case 'del':
      if (!variable_get('po_re_importer_textgroups', 0)) {
        return drush_set_error('DRUSH_PROJECT_ARGUMENT_INVALID', dt('Please enable the option "Clean up textgroups in database before reimport" in the Po-Re-Importer settings of your drupal website.'));
      }
      if (!($return = _po_re_importer_file_count(PO_RE_IMPORTER_IMPORTED, $langcode))) {
        return drush_set_error('DRUSH_PROJECT_ARGUMENT_INVALID', dt('There are no modules which have translation files in this language.'));
      }
    break;
    default:
      if (!($projectinfos = _po_re_importer_get_projects_infos($module))) {
        return drush_set_error('DRUSH_PROJECT_ARGUMENT_INVALID', dt('This module is not enabled.'));
      }
  }
}

function drush_po_re_importer_import($module = '', $langcode = '') {
  $form_id = 'po_re_importer_import_form';
  $form_state = array();

  $mode = drush_get_option('import_mode');
  switch ($mode) {
    case 'keep':
      $safe_mode = LOCALE_IMPORT_KEEP;
    break;
    case 'over':
      $safe_mode = LOCALE_IMPORT_OVERWRITE;
    break;
    default:
      $safe_mode = LOCALE_IMPORT_KEEP;
    break;
  }

  $group = drush_get_option('textgroup');
  if (empty($group)) {
    $safe_group = 'default';
  }
  else{
    $safe_group = $group;
  }

  $form_state['values']['import_mode'] = $safe_mode;
  $form_state['values']['components'] = $module;
  $form_state['values']['language'] = $langcode;
  $form_state['values']['textgroup'] = $safe_group;
  $form_state['values']['op'] = dt('Import');
  drupal_form_submit($form_id, $form_state);
}

